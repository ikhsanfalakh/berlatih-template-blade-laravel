@extends('layout.master')

@section('content')
<section class="content">
    <!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Data Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif
        <table id="example1" class="table table-bordered table-striped">
            <a href="/pertanyaan/create" class="btn btn-primary">Tambah</a>
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th style="width: 20%" scope="col">Judul</th>
                    <th style="width: 30%" scope="col">Isi</th>
                    <th style="width: 180px" scope="col">Tgl Insert</th>
                    <th style="width: 180px" scope="col">Tgl Update</th>
                    <th style="width: 350px">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pertanyaan as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->judul}}</td>
                    <td>{{$value->isi}}</td>
                    <td>{{$value->created_at}}</td>
                    <td>{{$value->updated_at}}</td>
                    <td>
                        <form action="/pertanyaan/{{$value->id}}" method="POST">
                            <a href="/pertanyaan/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="6" align="center">No data</td>
                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <th scope="col">#</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th>Tgl Insert</th>
                    <th>Tgl Update</th>
                    <th scope="col">Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
</section>
@endsection

@push('scripts')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function() {
        $("#example1").DataTable();
    });
</script>
@endpush
