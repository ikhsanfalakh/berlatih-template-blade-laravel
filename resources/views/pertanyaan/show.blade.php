@extends('layout.master')

@section('content')
<section class="content">
    <!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Detail Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <h4>{{$pertanyaan->judul}}</h4>
        <p>{{$pertanyaan->isi}}</p>
    </div>
    <!-- /.card-body -->
</div>
</section>
@endsection
