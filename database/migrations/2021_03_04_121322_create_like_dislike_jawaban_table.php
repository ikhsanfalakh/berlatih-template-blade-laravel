<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikeJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_jawaban', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('jawaban_id')->unsigned();
            $table->foreign('jawaban_id')->references('id')->on('jawaban');
            
            $table->integer('profil_id')->unsigned();
            $table->foreign('profil_id')->references('id')->on('profil');
            
            $table->unique(['jawaban_id','profil_id']);
            /*klo menggunakan primarykey ini, unique diatas ditutup
            increments id jg ditutup*/
            //$table->primary(['jawaban_id','profil_id']);

            $table->integer('poin');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_jawaban');
    }
}
