<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_pertanyaan', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('pertanyaan_id')->unsigned();
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaan');
            
            $table->integer('profil_id')->unsigned();
            $table->foreign('profil_id')->references('id')->on('profil');
            
            $table->unique(['pertanyaan_id','profil_id']);
            /*klo menggunakan primarykey ini, unique diatas ditutup
            increments id jg ditutup*/
            //$table->primary(['pertanyaan_id','profil_id']);

            $table->integer('poin');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_pertanyaan');
    }
}
